<?php
/**
 * The Scroll-Top / Back-To-Top Scrolling Button
 *
 * @package Limestone WordPress theme
 * @subpackage Partials
 * @version 3.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<a href="#" id="site-scroll-top"><span class="fa fa-chevron-up"></span></a>
