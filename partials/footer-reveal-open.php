<?php
/**
 * Open the footer reveal container
 *
 * @package Limestone WordPress theme
 * @subpackage Partials
 * @version 3.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div class="footer-reveal clr">
