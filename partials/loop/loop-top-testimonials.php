<?php
/**
 * Loop Top : Portfolio
 *
 * @package Limestone WordPress theme
 * @subpackage Partials
 * @version 3.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} ?>

<div id="testimonials-entries" class="<?php echo wpex_get_testimonials_wrap_classes(); ?>">
