<?php
/**
 * Blog entry avatar
 *
 * @package Limestone WordPress theme
 * @subpackage Partials
 * @version 3.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Define classes
$classes = 'blog-entry-header clr';
if ( $avatar_enabled = wpex_post_entry_author_avatar_enabled( get_the_ID() ) ) {
	$classes .= ' header-with-avatar';
} ?>

<header class="<?php echo $classes; ?>">

	<h2 class="blog-entry-title entry-title">
		<a href="<?php wpex_permalink(); ?>" title="<?php wpex_esc_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
	</h2><!-- .blog-entry-title -->

	<?php if ( $avatar_enabled ) : ?>

		<?php get_template_part( 'partials/blog/blog-entry-avatar' ); ?>
		
	<?php endif; ?>

</header><!-- .<?php $classes; ?> -->
