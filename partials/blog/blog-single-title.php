<?php
/**
 * Single blog post title
 *
 * @package Limestone WordPress theme
 * @subpackage Partials
 * @version 3.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Only display if the main header is set to custom text
if ( 'custom_text' == wpex_get_mod( 'blog_single_header', 'custom_text' ) ) : ?>

	<header class="single-blog-header clr">
		<h1 class="single-post-title entry-title"<?php wpex_schema_markup( 'headline' ); ?>><?php the_title(); ?></h1><!-- .single-post-title -->
	</header><!-- .blog-single-header -->

<?php endif; ?>
