<?php
/**
 * Mobile Icons Header Menu.
 *
 * @package Limestone WordPress Theme
 * @subpackage Partials
 * @version 3.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<?php
// Closing toggle for the sidr mobile menu style
if ( 'sidr' == wpex_global_obj( 'mobile_menu_style' ) ) : ?>

	<div id="sidr-close"><a href="#sidr-close" class="toggle-sidr-close"></a></div>

<?php endif; ?>

<div id="wpex-mobile-menu-fixed-top" class="clr wpex-hidden">
	<div class="container clr">
		<a href="#mobile-menu" class="mobile-menu-toggle"><?php echo apply_filters( 'wpex_mobile_menu_open_button_text', '<span class="fa fa-navicon"></span>' ); ?><?php echo wpex_get_mod( 'mobile_menu_toggle_text', _x( 'Menu', 'Mobile Menu Toggle Button Text', 'wpex' ) ); ?></a>
	</div><!-- .container -->
</div><!-- #mobile-menu -->
